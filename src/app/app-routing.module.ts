import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CandidatosComponent } from './candidatos/componentes/candidatos/candidatos.component';

const routes: Routes = [
  { path: 'test', component: CandidatosComponent },
  { path: '',   redirectTo: '/test', pathMatch: 'full' }, // redirect 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
