import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { candidatosReducer } from './candidatos-store/candidatos.reducer';
import { StoreModule } from '@ngrx/store';

import { CandidatosComponent } from './componentes/candidatos/candidatos.component'
import { AdministrarCandidatosComponent } from './componentes/administrar-candidatos/administrar-candidatos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CandidatosComponent,
    AdministrarCandidatosComponent
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ 'candidatos': candidatosReducer }),
  ],  
  
  exports: [
    CandidatosComponent,
    AdministrarCandidatosComponent
  ]
})

export class CandidatosModule { }
