import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { Candidato } from 'src/app/shared/models/candidato/candidato.model';
import { ListadoCandidatosVotos } from '../../candidatos-store/candidatos.reducer';
import { agregarCandidato, eliminarCandidato, incrementarVoto, decrementarVoto, reiniciarVotos } from '../../candidatos-store/candidatos.actions';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-administrar-candidatos',
  templateUrl: './administrar-candidatos.component.html',
  styleUrls: ['./administrar-candidatos.component.scss']
})
export class AdministrarCandidatosComponent implements OnInit {
  public name: string = '';
 
  // Cae en desuso con el uso de redux y actions, por lo tanto no fue usado 
  @Output()
  agregadoEvt = new EventEmitter<string>();

  public grupoForm : FormGroup;
  
  constructor( private store: Store<ListadoCandidatosVotos> ) {
    this.grupoForm = new FormGroup({
      nombre: new FormControl('', [ Validators.required ] )
    });
  }

  ngOnInit(): void {
  }

  public agregarCandidato () {
    // Solo si el formgroup es válido
    if ( this.grupoForm.valid ) {
      let candidato : Candidato = new Candidato( 0, this.grupoForm.get('nombre')?.value );
      this.store.dispatch(agregarCandidato({ candidato : candidato }));

      this.grupoForm.get('nombre')?.setValue('');
    }
  }

}
