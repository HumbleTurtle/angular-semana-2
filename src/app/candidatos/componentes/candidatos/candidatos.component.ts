import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Candidato } from 'src/app/shared/models/candidato/candidato.model';

import { agregarCandidato, eliminarCandidato, incrementarVoto, decrementarVoto, reiniciarVotos } from '../../candidatos-store/candidatos.actions';
import { ListadoCandidatosVotos } from '../../candidatos-store/candidatos.reducer';
import * as test from '../../candidatos.module'

@Component({
  selector: 'app-candidatos',
  templateUrl: './candidatos.component.html',
  styleUrls: ['./candidatos.component.scss']
})
export class CandidatosComponent implements OnInit {
  
  private state : ListadoCandidatosVotos | null;

  constructor( private store: Store<ListadoCandidatosVotos> ) {
    this.state = null;
 
  }

  ngOnInit(): void {
    var self = this;
    this.store.select(state => state.candidatos).subscribe( s => self.state = s );
  }
  
  public get candidatos() : Candidato[] {
    return Object.values(this.state?.candidatos);   
  }
  
  public incrementarVoto( clave: number ) {
    this.store.dispatch(incrementarVoto( {clave} ));
  }

  public decrementarVoto( clave: number ) {
    this.store.dispatch(decrementarVoto( {clave} ));
  }
  
  public borrarCandidato ( clave : number ) {
    this.store.dispatch(eliminarCandidato( { clave } ));
  }
  
}