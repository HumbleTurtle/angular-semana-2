import { createSelector } from '@ngrx/store';
import { ListadoCandidatosVotos } from './candidatos.reducer';


export const selectListado = (state: ListadoCandidatosVotos) => state;

export const selectCandidatos = createSelector(
    selectListado,
    (state) => state.candidatos
);