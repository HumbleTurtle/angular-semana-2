import { ActionCreator, ActionReducer, createReducer, on, State } from '@ngrx/store';
import { Action } from 'rxjs/internal/scheduler/Action';
import { agregarCandidato, eliminarCandidato, incrementarVoto, decrementarVoto, reiniciarVotos } from './candidatos.actions';
import { cloneDeep } from 'lodash';
import { Candidato } from 'src/app/shared/models/candidato/candidato.model';


export interface ListadoCandidatosVotos {
    index : number,
    elementos : number[],
    candidatos : any
}

export const initialState : ListadoCandidatosVotos = {
    index: 0,
    elementos: [],
    candidatos: {
    } 
};
 
const _reducer = createReducer(
  initialState,
  
  on(agregarCandidato, (state, { candidato }) => {  
    let candidatos = {...state.candidatos};
    let elementos = [...state.elementos];
    let index = state.index;

    console.log(candidato);
    
    candidatos[index] = candidato;
    elementos.push( index++);

    return { ...state, candidatos, elementos, index };
  }),

  on(eliminarCandidato, (state, { clave }) => {
    let candidatos = {...state.candidatos};
    let elementos = [...state.elementos];
    const index = elementos[clave]; 

    delete candidatos[index];
    elementos.splice( clave, 1 );

    return { ...state, candidatos, elementos };
  }),

  on( incrementarVoto, (state, { clave }) => {
    let elementos = [...state.elementos];
    const index = elementos[clave]; 

    let candidato : Candidato = {...state.candidatos[index]};

    candidato.votos += 1;

    return { ...state, candidatos: {...state.candidatos, [index]:candidato }};
  }),

  on( decrementarVoto, (state, { clave }) => {
    let elementos = [...state.elementos];
    const index = elementos[clave]; 

    let candidato : Candidato = {...state.candidatos[index]};

    if ( candidato.votos > 0 )
      candidato.votos -= 1;

    return { ...state, candidatos: {...state.candidatos, [index]:candidato }};
  }),

  on( reiniciarVotos, (state, { clave }) => {
    state.candidatos[clave].votos = 0;
    return state;
  })

);

export function candidatosReducer(state = initialState, action : any ) {
  return _reducer(state, action);
}