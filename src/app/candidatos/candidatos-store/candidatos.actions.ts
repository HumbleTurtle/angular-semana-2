import { createAction, props } from '@ngrx/store';
import { Candidato } from 'src/app/shared/models/candidato/candidato.model';


export const agregarCandidato = createAction('Agregar candidato', props<{ candidato: Candidato }>() );
export const eliminarCandidato = createAction('Eliminar candidato', props<{ clave: number }>() );

export const incrementarVoto = createAction('Incrementar voto candidato', props<{ clave: number }>() );
export const decrementarVoto = createAction('Decrementar voto candidato', props<{ clave: number }>() );

export const reiniciarVotos = createAction('Reset voto candidato', props<{ clave: number }>() );